//
//  tasktwoApp.swift
//  tasktwo
//
//  Created by mac on 15/10/20.
//

import SwiftUI

@main
struct tasktwoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
