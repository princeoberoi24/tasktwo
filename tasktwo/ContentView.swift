//
//  ContentView.swift
//  tasktwo
//
//  Created by mac on 15/10/20.
//

import SwiftUI

struct ContentView: View {
    
    @State var index = 0
    
    var body: some View {
       
        VStack(spacing: 0){
            
            ZStack{
                
                if self.index == 0{
                    
                    Image("backg").resizable().edgesIgnoringSafeArea(.top)
                    Text("Pancakes with less").font(.system(size: 24)).foregroundColor(Color.white).padding(.top, 350.0).padding(.leading,-185)
                    Text("guilt").font(.system(size: 24)).foregroundColor(Color.white)
                        .padding(.top, 392).padding(.trailing, 334)
                     
                    Text("This is a yummy pancake i encountered at Yummy Restaurant. They have a freshle made yogurt with baked bananas which made for a lip smacking breakfast.").foregroundColor(Color.white)
                        .lineLimit(4)
                        .padding(.top, 515)
                    
                    Button("Order Now") {
                               print("Button Tapped")
                    }
                        
                        .frame(width: 290, height: 39)
                        .background(Color.pink)
                    .foregroundColor(.white).cornerRadius(15).padding(.top, 660
                    ).padding(.leading,-95)
                    
                    
                }
                else if self.index == 1{
                    
                    Color.red.edgesIgnoringSafeArea(.top)
                }
                else if self.index == 2{
                    
                    Color.green.edgesIgnoringSafeArea(.top)
                }
                else{
                    
                    Color.blue.edgesIgnoringSafeArea(.top)
                }
                
            }
            .padding(.bottom, -35)
            
            CustomTabs(index: self.$index)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct CustomTabs : View {
    
    @Binding var index : Int
    
    var body : some View{
        
        HStack{
            
            Button(action: {
                
                self.index = 0
                
            }) {
                
                Image(systemName: "house").font(.system(size: 30, weight: .regular))
                    
                
                
            }
            .foregroundColor(Color.black.opacity(self.index == 0 ? 1 : 0.2))
                
           
            
            Spacer(minLength: 0)
            
            
            Button(action: {
                
                self.index = 1
                
            }) {
                
                Image(systemName: "magnifyingglass").font(.system(size: 30, weight: .regular))
                
            }
            .foregroundColor(Color.black.opacity(self.index == 1 ? 1 : 0.2))
            .offset(x: 10)
            
            Spacer(minLength: 0)
            
            Button(action: {
                
            }) {
                
                Image(systemName: "plus.circle.fill").font(.system(size: 60, weight: .regular)).overlay(Circle().stroke(Color.black, lineWidth: 10))
            }
            .offset(y: -25)
            
            Spacer(minLength: 0)
            
            Button(action: {
                
                self.index = 2
                
            }) {
                
                Image(systemName: "bag").font(.system(size: 30, weight: .regular))
                
            }
            .foregroundColor(Color.black.opacity(self.index == 2 ? 1 : 0.2))
            .offset(x: -10)
            
            Spacer(minLength: 0)
            
            Button(action: {
                
                self.index = 3
                
            }) {
                
                Image(systemName: "person").font(.system(size: 30, weight: .regular))
                
            }
            .foregroundColor(Color.black.opacity(self.index == 3 ? 1 : 0.2))
        }
        .padding(.horizontal, 35)
        .padding(.top, 35)
        .background(Color.white)
        .clipShape(CShape())
        
       
    }
}

struct CShape : Shape {
    
    func path(in rect: CGRect) -> Path {
        
        return Path{path in
            
            path.move(to: CGPoint(x: 0, y: 35))
            path.addLine(to: CGPoint(x: 0, y: rect.height))
            path.addLine(to: CGPoint(x: rect.width, y: rect.height))
            path.addLine(to: CGPoint(x: rect.width, y: 35))
            
            path.addArc(center: CGPoint(x: (rect.width / 2) + 5, y: 41), radius: 35, startAngle: .zero, endAngle: .init(degrees: 180), clockwise: true)
            
            
        }
    }
}
